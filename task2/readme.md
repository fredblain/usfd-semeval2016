Folder for task2.


TASK STRUCTURE:

1) PHRASE2VEC
	INPUT: 
	1) pretrained Google phrase vectors https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing
	2) 	1.  chunk sentence X for sentence in N
			1. a. OpenNLP ( https://opennlp.apache.org/cgi-bin/download.cgi )
		2.	create phse2vec based on data
		
	Testing data: 
	SemEval2015 (both train and test data)

2) DBPedia ontology

	INPUT:
	1.  chunk sentence X for sentence in N
		1. a. OpenNLP ( https://opennlp.apache.org/cgi-bin/download.cgi )
	2. AUSE DBPEDIA TO LABLE CHUNK WITH ONTOLOGICAL CLASSES
	
	TOOLS:
		https://rdflib.github.io/sparqlwrapper/
		https://github.com/ubergrape/pyspotlight
		
3) using syntactic decomposition tree information evaluate the POS similarities between S1 and S2.


EVALUTE ON EACH MASURE INDIVIDUALY AND COMBINE THEM IN TO A SINGLE SCORE VALUE.  
		
	