Call for Papers
-------------------

SemEval-2016 invites papers that describe participating systems (for task participants) and tasks (for task organizers).  The only accepted formats for submitted papers is PDF. Papers should be submitted using the [START system][1].

Submissions should use the ACL LaTeX style files or Microsoft Word style files tailored for the NAACL 2016 conference, with which SemEval-2016 will be co-located; these style files are available [here][2].

Paper submissions must be electronically submitted in PDF format and must conform to the official style guidelines provided in the PDF file [here][3].

Notes: ***paper reviewing will NOT be blind***, so you can include authors' names, affiliations, and references in the submitted paper.



**SemEval-2016 System Description Paper**

Papers should describe the methods used clearly, and in sufficient detail in order to ensure reproducibility.

The system papers will be reviewed by other task participants in the same task. We expect that task participants are happy to review papers within the same task.

+ Paper Title:  
The title should follow the pattern `"<TEAM_ID> :  <Paper Title>"`  where `<TEAM_ID>` is the team ID used when registering with SemEval-2016 and `<Paper Title>` is the title of your paper.

+ Page Limit:  
A system description paper has a recommended length of 4 pages of content, with a maximum length of 6 pages, should the authors need include addition analyses or descriptions.  There will be no limit on the number of pages for references.

[1]: http://softconf.com/naacl2016/SemEval2016/user/
[2]: http://naacl.org/naacl-pubs/
[3]: http://naacl.org/naacl-pubs/assets/files/naaclhlt2016-latex/naaclhlt2016.pdf
