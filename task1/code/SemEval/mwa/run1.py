__author__ = 'MarinaFomicheva'

from aligner import *
import argparse

def parse_args():
    """Parse command line arguments"""
    parser = argparse.ArgumentParser(
            )

    parser.add_argument("--sent1",
            help="Tokenized sentence 1",
            required=True,
            type=str
            )

    parser.add_argument("--sent2",
            help="Tokenized sentence 2",
            required=True,
            type=str
            )

    return parser.parse_args()

def run1(s1, s2):

    words1 = s1.strip().split(' ')
    words2 = s2.strip().split(' ')
    content = {}
    aligned = {}

    alignments = align(s1, s2)

    for i, s in enumerate([words1, words2]):
        content[i] = 0.0
        aligned[i] = 0.0
        for word in s:
            if word.lower() not in stopwords and word.lower() not in punctuations:
                content[i] += 1
                if word in [x[i] for x in alignments[1]]:
                    aligned[i] += 1

    sem_sim = (aligned[0] + aligned[1])/(content[0] + content[1])

    return sem_sim


def main():

    config = parse_args()

    sem_sim = run1(config.sent1, config.sent2)
    print(str(sem_sim))

if __name__ == '__main__':
    main()
