/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.semeval;

import com.shef.baselines.CosineWordBased;
import com.shef.baselines.SimilarityMeasure;
import com.shef.cmdCalls.CMDCaller;
import com.shef.constants.Constants;
import com.shef.representation.Sentence;
import com.shef.util.DataProcessor;
import com.shef.util.Langs;
import com.shef.util.LanguageProcessor;
import com.shef.util.Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

/**
 *
 * @author ahmet
 */
public class Task1SubTask1 {

    public static void main(String args[]) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        LanguageProcessor.setUp(Langs.EN, "nlpModels/");

        Vector<SimilarityMeasure> metrics = new Vector<SimilarityMeasure>();

        //Vector<String> configuration = Util.getFileContentAsVector("featureConfiguration.txt");
        Properties propFeatures = new Properties();
        InputStream in = new FileInputStream("featureConfiguration.properties");
        propFeatures.load(in);
        in.close();
        Properties propSystemSetting = new Properties();
        in = new FileInputStream("setUpConfiguration.properties");
        propSystemSetting.load(in);
        in.close();

        Iterator<Object> enumeration = propFeatures.keySet().iterator();
        while (enumeration.hasNext()) {
            String key = (String) enumeration.next();
            String value = propFeatures.getProperty(key);
            SimilarityMeasure metric = (SimilarityMeasure) Class.forName(value).newInstance();
            metric.setID(key);
            String pathToModel = propSystemSetting.getProperty(key + Constants.FEATUREMODEL);
            metric.setModelPath(pathToModel);
            metrics.add(metric);
        }

        File testingTask = new File(propSystemSetting.getProperty(Constants.TASK) + "/allData");

        File testingDataFiles[] = new File(testingTask.getAbsolutePath() + "/Testing" + propSystemSetting.getProperty(Constants.TESTING_INPUT)).listFiles();

        for (int j = 0; j < testingDataFiles.length; j++) {
            File file = testingDataFiles[j];
            String name = file.getName();
            Vector<String> sentencePairs = Util.getFileContentAsVectorUTF(file.getAbsolutePath());
            if (name.contains("input")) {
                Vector<String> testingPairs = new Vector<String>();
                Vector<String> goldInstances = new Vector<String>();
                String newPath = file.getAbsolutePath().replaceAll(".input.", ".gs.");
                Vector<String> goldForPairs = Util.getFileContentAsVector(newPath);
                for (int k = 0; k < goldForPairs.size(); k++) {
                    String dataUnit = goldForPairs.get(k);
                    if (!"".equals(dataUnit.trim())) {
                        goldInstances.add(dataUnit);
                        testingPairs.add(sentencePairs.get(k));
                    }
                }

                for (int i = 0; i < metrics.size(); i++) {
                    SimilarityMeasure metric = metrics.get(i);
                    StringBuffer output = new StringBuffer();
                    Vector<Double> systemOutputs = new Vector<Double>();
                    for (int k = 0; k < testingPairs.size(); k++) {
                        String sentencePair[] = testingPairs.get(k).split("\t");
                        Sentence sentence1 = DataProcessor.processText(sentencePair[0]);
                        Sentence sentence2 = DataProcessor.processText(sentencePair[1]);
                        double sim = metric.getSimilarity(sentence1, sentence2);
                        System.out.println(sim);
                        systemOutputs.add(sim);
                        //System.out.println(metric.getId() + "\t" + sim);
                        output.append(sim).append("\n");
                    }

                    File folderToCreate = new File(propSystemSetting.getProperty(Constants.TASK) + "/systemOutputs" + "/Testing" + propSystemSetting.getProperty(Constants.TESTING_INPUT));
                    folderToCreate.mkdir();
                    folderToCreate = new File(folderToCreate.getAbsolutePath() + "/" + metric.getId());
                    if (folderToCreate.exists()) {
                        File filesInThere[] = folderToCreate.listFiles();
                        for (int k = 0; k < filesInThere.length; k++) {
                            File fileInThere = filesInThere[k];
                            fileInThere.delete();
                        }
                    }
                    folderToCreate.mkdir();
                    Util.doSave(folderToCreate.getAbsolutePath() + "/" + "Testing" + propSystemSetting.getProperty(Constants.TESTING_INPUT) + "." + name + "." + metric.getId(), output.toString());

                    //prepare system + ref files
                    StringBuffer system = new StringBuffer();
                    StringBuffer ref = new StringBuffer();

                    for (int m = 0; m < goldInstances.size(); m++) {
                        String goldInstance = goldInstances.get(m);
                        system.append(systemOutputs.get(m)).append("\n");
                        ref.append(goldInstance).append("\n");
                    }

                    Util.doSave((new File(propSystemSetting.getProperty(Constants.TASK))).getAbsolutePath() + "/scripts/system.txt", system.toString());
                    Util.doSave((new File(propSystemSetting.getProperty(Constants.TASK))).getAbsolutePath() + "/scripts/ref.txt", ref.toString());

//            StringBuffer scriptCall = new StringBuffer();
//            scriptCall.append("cd " + (new File("scripts")).getAbsolutePath()).append("\n");
//            scriptCall.append("./correlation ").append("ref.txt").append(" ");
//            scriptCall.append("system.txt");
//
//            Util.doSave(propSystemSetting.getProperty(Constants.TASK) + "/scripts/callCorrelation.sh", scriptCall);
                    //now call perl script to compute correlation
//            CMDCaller.callScript((new File(propSystemSetting.getProperty(Constants.TASK))).getAbsolutePath() + "/scripts/chPermission.sh");
                    CMDCaller.callScript((new File(propSystemSetting.getProperty(Constants.TASK))).getAbsolutePath() + "/scripts/callCorrelation.sh");
                    File result = new File((new File(propSystemSetting.getProperty(Constants.TASK))).getAbsolutePath() + "/scripts/result." + name + ".txt");

                    if (result.exists()) {
                        String resultStr = Util.getFileContentAsBuffer(result.getAbsolutePath()).toString();
                        resultStr = resultStr.replaceAll(".*:", "").trim();
                        Util.doSave(folderToCreate.getAbsolutePath() + "/" + "Testing" + propSystemSetting.getProperty(Constants.TESTING_INPUT) + "." + metric.getId() + ".pearson", resultStr);
                    }

                }
            }

        }
    }
}
