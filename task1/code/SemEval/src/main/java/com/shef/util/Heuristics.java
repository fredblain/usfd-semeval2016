/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.simmetrics.StringMetric;
import org.simmetrics.builders.StringMetricBuilder;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.simplifiers.Simplifiers;
import org.simmetrics.tokenizers.Tokenizers;

/**
 *
 * @author ahmetaker
 */
public class Heuristics {

    public static Map<String, Double> IDF = null;

    private static String LANG = "en";

    public static void setResourceLang(String aLang) {
        LANG = aLang;
    }

    public static void prepareIDF(String aPath) throws IOException {
        IDF = new HashMap<String, Double>();
        IDF = Util.getFileContentAsMapInDoubleValue(aPath, "\t");
    }

    public static boolean isAChar(String aChar) {
        String pattern = "[a-z]";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = r.matcher(aChar);
        return m.matches();
    }

    public static boolean isPunctuation(String aWord) {
        String pattern = "\\p{Punct}+$";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(aWord);
        boolean match = m.matches();
        if (!match) {
            if (aWord.equals("\"") || aWord.equals("[") || aWord.equals("]")
                    || aWord.equals("{") || aWord.equals("}") || aWord.equals("(") || aWord.equals(")")
                    || aWord.equals("~") || aWord.equals("#") || aWord.equals("'") || aWord.equals("`")
                    || aWord.equals("*") || aWord.equals("’") || aWord.equals("“") || aWord.equals("„")
                    || aWord.equals("≥") || aWord.equals(">") || aWord.equalsIgnoreCase("<")) {
                return true;
            }
        }
        return match;
    }

    public static boolean endsWithPunction(String aWord) {
        String pattern = "\\p{Punct}+$";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(aWord.charAt(aWord.length() - 1) + "");
        return m.matches();
    }

    public static boolean startsWithPunction(String aWord) {
        String pattern = "\\p{Punct}+$";
        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        Matcher m = r.matcher(aWord.charAt(0) + "");
        return m.matches();
    }

    public static boolean isNumber(String aWord) {
        Pattern p = Pattern.compile("\\d+,\\d+");
        Matcher m = p.matcher(aWord);
        Pattern p2 = Pattern.compile(",\\d+");
        Matcher m2 = p2.matcher(aWord);
        Pattern p3 = Pattern.compile("\\d+\\.\\d+");
        Matcher m3 = p3.matcher(aWord);
        Pattern p4 = Pattern.compile("\\.\\d+");
        Matcher m4 = p4.matcher(aWord);
        Pattern p5 = Pattern.compile("^[-+]?\\d+(\\.\\d+)?$");
        Matcher m5 = p5.matcher(aWord);
        return (m.find() || m2.find() || m3.find() || m4.find() || m5.find());
    }

    public static boolean isStrictNumber(String aWord) {
        Pattern p = Pattern.compile("\\d+.\\d+");
        Matcher m = p.matcher(aWord);
        Pattern p2 = Pattern.compile("\\d+");
        Matcher m2 = p2.matcher(aWord);
        return (m.matches() || m2.matches());
    }

    public static boolean isAllCapital(String aWord) {
        Pattern p = Pattern.compile("\\b([A-Z])?(\\p{Lu})?[A-Z\\p{Lu}]+\\b");
        Matcher m = p.matcher(aWord);
        return (m.matches());
    }

    public static boolean isFirstCharCapital(String aWord) {
        return Character.isUpperCase(aWord.charAt(0));
    }

    public static boolean foundNounSequenceThree(String aSentence) {
        Pattern p = Pattern.compile("(\\w)+,(\\s)+(\\w)+,(\\s)+(\\w)+");
        Matcher m = p.matcher(aSentence);
        return m.find();
    }

    public static boolean foundNounSequenceTwo(String aSentence) {
        Pattern p = Pattern.compile("(\\w)+,(\\s)+(\\w)+");
        Matcher m = p.matcher(aSentence);
        return m.find();
    }

    public static double getCosineSimilarity(String aTextA, String aTextB) {

        StringMetric metric = StringMetricBuilder.with(new CosineSimilarity<String>())
                .simplify(Simplifiers.toLowerCase(Locale.ENGLISH))
                .simplify(Simplifiers.replaceNonWord())
                .tokenize(Tokenizers.whitespace())
                .build();

        return metric.compare(aTextB, aTextB);
    }


    public static void main(String args[]) throws IOException {

        double score = Heuristics.getCosineSimilarity("ahmet;ahmet;ahmet", "ahmet;ahmet;ahmet");
        System.out.println(score);
    }
}
