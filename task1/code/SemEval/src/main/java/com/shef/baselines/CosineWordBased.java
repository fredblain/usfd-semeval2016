/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.baselines;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.shef.representation.Sentence;
import com.shef.util.Heuristics;

/**
 *
 * @author ahmetaker
 */
public class CosineWordBased extends SimilarityMeasure {

    @Override
    public double getSimilarity(Sentence aTextA, Sentence aTextB) {
        setAchievedScore(Heuristics.getCosineSimilarity(aTextA.getSentenceText(), aTextB.getSentenceText()));
        return getAchievedScore();
    }
    
    

}
