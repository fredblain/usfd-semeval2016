/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.constants;

/**
 *
 * @author ahmet
 */
public class Constants {
    
    public static String FEATUREMODEL = "ModelLocation";
    
    public static String TESTING_INPUT = "testingInput";
    
    public static String TESTING_OUTPUT = "testingOutput";

    public static String TASK = "task";
    
    public static String LOCAL_MACHINE_SETUP = "localMachineSetup";
}
