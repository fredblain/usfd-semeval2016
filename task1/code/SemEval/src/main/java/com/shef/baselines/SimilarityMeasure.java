/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.shef.baselines;

import com.shef.representation.Sentence;

/**
 *
 * @author ahmetaker
 */
public abstract class SimilarityMeasure {
    
    private double itsWeigth;
    private double itsAchievedScore;
    private String itsID = null;
    private String itsModelPath = null;
    
    public void setModelPath(String aModelPath) {
        itsModelPath = aModelPath;
    }
    
    public String getModelPath() {
        return itsModelPath;
    }
    
    public void setID(String anId) {
        itsID = anId;
    }
    
    public String getId() {
        return itsID;
    }
    
    public double getAchievedScore() {
        return itsAchievedScore;
    }
    
    public void setAchievedScore(double anAchievedScore) {
        itsAchievedScore = anAchievedScore;
    }
    
    public double getWeight() {
        return itsWeigth;
    }
    
    public void setWeight(double aWeight) {
        itsWeigth = aWeight;
    }
    
    public abstract double getSimilarity(Sentence aTextA, Sentence aTextB);
    
        
}
