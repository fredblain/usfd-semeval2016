/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.util;

import com.shef.representation.Sentence;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 *
 * @author ahmetaker
 */
public class DataProcessor {

    private static int id = 0;

    public static Sentence processText(String aText) throws IOException {
        String sentence = aText;

        String tokens[] = LanguageProcessor.tokenize(sentence);

        String nes[] = LanguageProcessor.getNE(tokens);
        String pos[] = LanguageProcessor.posTag(tokens);

        Sentence sentenceObj = new Sentence(sentence, (id++) + "", nes, pos, tokens);
        return sentenceObj;
    }

}
