/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shef.baselines;

import com.shef.cmdCalls.CMDCaller;
import com.shef.representation.Sentence;
import com.shef.util.Util;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ahmet
 */
public class MWA extends SimilarityMeasure {

    public double getSimilarity(Sentence aTextA, Sentence aTextB) {
        //python.sim(aText.getText(), aTextB.getText());
        StringBuffer call = new StringBuffer("cd /home/ahmet/usfd-semeval/usfd-semeval2016/task1/code/SemEval/mwa/\n");
        try {
            call.append("python run1.py --sent1 \" " + aTextA.getTokensSeperatedBySemicolon().replaceAll(";", " ") + " \"");
            call.append(" --sent2 \" " + aTextB.getTokensSeperatedBySemicolon().replaceAll(";", " ")).append(" \" > result.xt");
            Util.doSave("/home/ahmet/usfd-semeval/usfd-semeval2016/task1/code/SemEval/mwa/callMWA.sh", call.toString());
            CMDCaller.callScript("/home/ahmet/usfd-semeval/usfd-semeval2016/task1/code/SemEval/mwa/mwaPermission.sh");
            CMDCaller.callScript("/home/ahmet/usfd-semeval/usfd-semeval2016/task1/code/SemEval/mwa/callMWA.sh");
            String result = Util.getFileContentAsBuffer("/home/ahmet/usfd-semeval/usfd-semeval2016/task1/code/SemEval/mwa/result.xt").toString();
            result = result.trim();
            if (result.equals("")) {
                return 0;
            }
            return (new Double(result)).doubleValue();
        } catch (IOException ex) {
            Logger.getLogger(MWA.class.getName()).log(Level.SEVERE, null, ex);
        }

        return -1;
    }

}
