package com.shef.cmdCalls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class StreamReaderSeveralEvals {

    private InputStream itsStream;
    private String aType;
    private Map<String, Vector<String>> itsROUGEValues = new HashMap<String, Vector<String>>();

    public StreamReaderSeveralEvals() {
    }

    StreamReaderSeveralEvals(InputStream aStream, String aType) {
        this.itsStream = aStream;
        this.aType = aType;
    }

    /**
     * @return the rouge1Value
     */
    public final Vector<String> getRouge1Value() {
        return itsROUGEValues.get("1");
    }

    /**
     * @return the rouge2Value
     */
    public final Vector<String> getRouge2Value() {
        return itsROUGEValues.get("2");
    }

    /**
     * @return the rOUGESU
     */
    public final Vector<String> getROUGESU() {
        return itsROUGEValues.get("SU");
    }

    /**
     * @return the rouge3Value
     */
    public final Vector<String> getRouge3Value() {
        return itsROUGEValues.get("3");
    }

    /**
     * @return the rouge4Value
     */
    public final Vector<String> getRouge4Value() {
        return itsROUGEValues.get("4");
    }

    /**
     * @return the rougeValue
     */
    public final Vector<String> getRougeLValue() {
        return itsROUGEValues.get("L");
    }

    public void startMe2() {
        try {
            InputStreamReader isr = new InputStreamReader(itsStream);
            BufferedReader br = new BufferedReader(isr);
            String line = null;

            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();
            isr.close();
            itsStream.close();

        } catch (Exception e) {
        }

    }

    public void startMe() {
        try {
            InputStreamReader isr = new InputStreamReader(itsStream);
            BufferedReader br = new BufferedReader(isr);
            String line = null;

            while ((line = br.readLine()) != null) {
                if (line.contains("ROUGE-L Eval")) {
                    line = line.replaceAll(".*ROUGE-L Eval", "");
                    line = line.replaceAll("P.*", "").trim();
                    String id = line.replaceAll("R.*", "").replaceAll("\\..*", "").trim();
                    String rougeScore = line.replaceAll(".*R:", "").trim();
                    Vector<String> list = itsROUGEValues.get("L");
                    if (list == null) {
                        list = new Vector<String>();
                    }
                    list.add(id + ":" + rougeScore);
                    itsROUGEValues.put("L", list);
                    
                } else if (line.contains("ROUGE-1 Eval")) {
                    line = line.replaceAll(".*ROUGE-1 Eval", "");
                    line = line.replaceAll("P.*", "").trim();
                    String id = line.replaceAll("R.*", "").replaceAll("\\..*", "").trim();
                    String rougeScore = line.replaceAll(".*R:", "").trim();
                    Vector<String> list = itsROUGEValues.get("1");
                    if (list == null) {
                        list = new Vector<String>();
                    }
                    list.add(id + ":" + rougeScore);
                    itsROUGEValues.put("1", list);

                } else if (line.contains("ROUGE-2 Eval")) {
                    line = line.replaceAll(".*ROUGE-2 Eval", "");
                    line = line.replaceAll("P.*", "").trim();
                    String id = line.replaceAll("R.*", "").replaceAll("\\..*", "").trim();
                    String rougeScore = line.replaceAll(".*R:", "").trim();
                    Vector<String> list = itsROUGEValues.get("2");
                    if (list == null) {
                        list = new Vector<String>();
                    }
                    list.add(id + ":" + rougeScore);
                    itsROUGEValues.put("2", list);

                } else if (line.contains("ROUGE-3 Eval")) {
                    line = line.replaceAll(".*ROUGE-3 Eval", "");
                    line = line.replaceAll("P.*", "").trim();
                    String id = line.replaceAll("R.*", "").replaceAll("\\..*", "").trim();
                    String rougeScore = line.replaceAll(".*R:", "").trim();
                    Vector<String> list = itsROUGEValues.get("3");
                    if (list == null) {
                        list = new Vector<String>();
                    }
                    list.add(id + ":" + rougeScore);
                    itsROUGEValues.put("3", list);

                } else if (line.contains("ROUGE-4 Eval")) {
                    line = line.replaceAll(".*ROUGE-4 Eval", "");
                    line = line.replaceAll("P.*", "").trim();
                    String id = line.replaceAll("R.*", "").replaceAll("\\..*", "").trim();
                    String rougeScore = line.replaceAll(".*R:", "").trim();
                    Vector<String> list = itsROUGEValues.get("4");
                    if (list == null) {
                        list = new Vector<String>();
                    }
                    list.add(id + ":" + rougeScore);
                    itsROUGEValues.put("4", list);

                } else if (line.contains("ROUGE-SU4 Eval")) {
                    line = line.replaceAll(".*ROUGE-SU4 Eval", "");
                    line = line.replaceAll("P.*", "").trim();
                    String id = line.replaceAll("R.*", "").replaceAll("\\..*", "").trim();
                    String rougeScore = line.replaceAll(".*R:", "").trim();
                    Vector<String> list = itsROUGEValues.get("SU");
                    if (list == null) {
                        list = new Vector<String>();
                    }
                    list.add(id + ":" + rougeScore);
                    itsROUGEValues.put("SU", list);
                }
            }

            br.close();
            isr.close();
            itsStream.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
