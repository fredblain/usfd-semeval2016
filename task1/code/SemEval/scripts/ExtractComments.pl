###v0.4 (19 Nov 2014) - Monica Lestari Paramita
###This version captures the time each comment was written at.

use strict;
use warnings;
use HTML::Entities;

#my $input = "DiscussionPageJSON.txt";
#my $htmlInput = "DiscussionPageFromJSON.txt.tmp";
#my $output = "output.txt";

my $input = shift;
my $htmlInput = $input . ".tmp";
my $output = shift;

open INPUT, "<:encoding(utf-8)", $input or die "Cannot open file $input: $!\n";
open OUTPUT, ">:utf8", $htmlInput or die "Cannot open file $output: $!\n";

while (<INPUT>) {
	my $content = $_;
	$content =~ s/^\{\"response\": \"//;
	$content =~ s/\"}$//;
	$content =~ s/\\n/\n/g;
	$content =~ s/\\//g;

	print OUTPUT $content;	
}
close INPUT;
close OUTPUT;

open INPUT, "<:encoding(utf-8)", $htmlInput or die "Cannot open file $input: $!\n";

open OUTPUT, ">:utf8", $output or die "Cannot open file $output: $!\n";
print OUTPUT "No\tCommentID\tReplyTo\tUsername\tRecommend\tDate\tComment\n";
my $recommend = "";
my $comment = "";
my $no = 1;
my $commentFlag = 0;
my $commentID = "";
my $replyTo = "";
my $username = "";
my $date = "";

#reply to:
#      <a class="d2-in-reply-to" href="#comment-37580283" data-link-internal
#         data-action="comment.highlight" data-action-target="37580283" title="View LondonRevisiteds comment, 30 June 2014 12:10am">LondonRevisited</a>

while (<INPUT>) {
	chomp($_);
	my $line = decode_entities($_);

	#if ($line =~ m/<div class=\"d2-recommend-count\" title=\"Recommended by ([0-9]+) people\">/) {

	if ($line=~ m/<a class="d2-username".*?>(.*?)<\/a>/) {
		$username = $1;
    }
	if ($line=~ m/<a data-action="comment.recommend" data-action-target="([0-9]+)".*?>/) {
		$commentID = $1;
	}

	if ($line =~ m/<b data-action="comment.recommend" data-action-target="([0-9]+)">([0-9]+)<\/b>/) {
		$commentID = $1;
		$recommend = $2;
	}
	elsif ($line =~ m/<div class=\"d2-recommend-count.*?\" title=\"Recommended by ([0-9]+) (people|person)\">/) {
		$recommend = $1;
	}
	elsif ($line =~ m/<div class=\"d2-recommend-count.*?\" title=\"No-one has recommended this comment">/) {
		$recommend = 0;
	}

	#if ($line =~ m/title=\"Link to this comment\">([0-9a-zA-z: ]+)</) {
	if ($line =~ m/title=\"Link to this comment\">(.*?)</) {
		#print $1 . "\n";
		$date = $1;
	}
	
	if ($line =~ m/<a class="d2-in-reply-to" href="#comment-([0-9]+)"/) {
		$replyTo = $1;
	}
	if (($commentFlag == 1) && ($line =~ m/<p>/)) {
		$comment = $line;
		$comment =~ s/<[\/]*[pbi]+>//g;
		$comment =~ s/<br \/>//g;
		$comment =~ s/^[\s]+//g;
		
		#if ($replyTo ne "") {
		#	print OUTPUT "\t";
		#}
		print OUTPUT $no . "\t" . $commentID . "\t" . $replyTo . "\t" . $username . "\t" . $recommend . "\t" . $date . "\t" . $comment . "\n";
		$commentFlag = 0;
		$recommend = "";
		$comment = "";
		$commentID = "";
		$replyTo = "";
		$username = "";
		
		$no++;
		
	}
	if ($line =~ m/<div class=\"d2-body\">/) {
		#print "Setting comment flag to 1.\n";
		$commentFlag = 1;
	}

}
close INPUT;
close OUTPUT;
unlink($htmlInput);
